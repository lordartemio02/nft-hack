import { NavigationItem } from "../types";

export const items: NavigationItem[] = [
  { to: "/", text: "Главная" },
  { to: "/info", text: "Инфо" },
  { to: "/afisha", text: "Афиша" },
  { to: "/myTickets", text: "Мои билеты" },
  { to: "/profile", text: "Профиль" },
  { to: "/admin", text: "Админ" },
];
