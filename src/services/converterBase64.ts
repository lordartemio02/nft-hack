export const converterToBase64 = (base64String: string) => {
  const base64Code = btoa(base64String);
  return base64Code;
};

export const converterFromBase64 = (base64Code: string) => {
  const base64String = atob(base64Code);
  return base64String;
};
