import { NavigationItem } from "../types";

export const mainTabItems: NavigationItem[] = [
  { to: "/afisha", text: "Афиша" },
  { to: "/myTickets", text: "Мои билеты" },
  { to: "/profile", text: "Профиль" },
];
