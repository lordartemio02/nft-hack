import axios from "axios";
import { getHashParam } from "../services";
import { CreateOrder, Sector } from "./Api.interfaces";

export const url = "https://nft-api.maxinteam.ru";

const testLink =
  "vk_access_token_settings=&vk_app_id=51557822&vk_are_notifications_enabled=0&vk_is_app_user=1&vk_is_favorite=0&vk_language=ru&vk_platform=desktop_web&vk_ref=other&vk_ts=1676721027&vk_user_id=142235120&sign=L0jmhDceDMNwSXum5rJsC7tRAMGdnJOb_b_G3iwYuow";

const prodLink = window.location.search.substring(1);

export const headers = {
  "X-Auth": testLink,
};

export const startAuth = async () => {
  try {
    const response = await axios.get(url + "/user/auth", {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const authUser = async () => {
  try {
    const response = await axios.get(url + "/org/auth", {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const getWalletsUser = async () => {
  try {
    const response = await axios.get(url + "/user/wallets", {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const createEvent = async (name: string, description: string) => {
  try {
    const response = await axios.post(
      url + "/org/events",
      { name, description },
      {
        headers: headers,
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const updateEvents = async (name: string, description: string) => {
  try {
    const response = await axios.post(
      url + "/org/events",
      { name, description },
      {
        headers: headers,
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const sectorCreate = async (props: Sector) => {
  try {
    const response = await axios.post(url + "/org/sector", props, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const sectorUpdate = async (props: Sector) => {
  try {
    const response = await axios.post(url + "/org/sector", props, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const sectors = async () => {
  try {
    const response = await axios.get(url + "/org/sector", {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const orderCreate = async (props: CreateOrder) => {
  const { event_id, sectoredIds } = props;
  const arraySectors: { id: number; count: number }[] = [];
  sectoredIds.forEach((item, i) => {
    return arraySectors.push({ id: item.id, count: item.count });
  });

  try {
    const response = await axios.post(
      url + "/user/orders",
      {
        event_id: event_id,
        sectors: arraySectors,
      },
      {
        headers: headers,
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const getTickets = async () => {
  try {
    const response = await axios.get(url + "/user/tickets", {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};

export const getTicket = async (id: number) => {
  try {
    const response = await axios.get(url + `/user/tickets/${id}`, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};
