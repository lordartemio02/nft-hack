export interface Sector {
  name: string;
  description: string;
  max_count: string;
  price: number;
  is_use_price: number;
  ticket_source_image: string;
  ticket_generate_props: [];
}

export interface Events {
  events: IEvent[];
  category: Category;
}

export interface EventResponse {
  data: {
    data: IEvent;
  };
}

export interface Category {
  id: number;
  name: string;
  code: string;
}

export interface IEvent {
  id: number;
  name: string;
  description: string;
  category: Category;
  date_start: Date;
  date_end: null;
  date_type: string;
  location: Location;
  image: string;
  is_public: boolean;
  is_active: boolean;
  sectors: Sectors[];
  oganizer: Organizer;
}

export interface Organizer {
  id: number;
  name: string;
  user: User;
}

export interface User {
  id: number;
  vk_id: string;
}
export interface Sectors {
  id: number;
  name: string;
  description: string;
  max_count: number;
  price: string;
  price_currency: PriceCurrency;
  ticket_source_image: string;
  ticket_generate_props: any[];
}

export interface PriceCurrency {
  name: string;
  code: string;
}

export interface Location {
  id: number;
  name: string;
  address: string;
  city: Category;
}

export interface CreateOrder {
  event_id: number;
  sectoredIds: { id: number; count: number }[];
}
