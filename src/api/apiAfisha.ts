import axios from "axios";
import { headers, url } from "./apiUser";
import { EventResponse, Events } from "./Api.interfaces";

export const getEvents = async () => {
  try {
    const response = await axios.get(url + "/afisha/events", {
      headers: headers,
    });
    return response.data;
  } catch (error) {
    return error;
  }
};

export const getEvent = async (id: number) => {
  try {
    const response: EventResponse = await axios.get(
      url + `/afisha/events/${id}`,
      {
        headers: headers,
      }
    );

    return response.data.data;
  } catch (error) {
    return error;
  }
};
