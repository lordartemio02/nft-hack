import { push, replace } from "@cteamdev/router";
import {
  Icon20ChevronRight,
  Icon20MessageOutline,
  Icon20TicketOutline,
  Icon24Filter,
  Icon28AddOutline,
} from "@vkontakte/icons";
import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Banner,
  Button,
  CellButton,
  ContentCard,
  Div,
  Group,
  Header,
  HorizontalCell,
  HorizontalScroll,
  Link,
  Search,
  SimpleCell,
  Text,
  Title,
} from "@vkontakte/vkui";
import React, { useEffect, useState } from "react";
import { Events } from "../../api/Api.interfaces";
import { getEvents } from "../../api/apiAfisha";
import CustomSearch from "../CustomSearch";
import { useHistory } from "react-router-dom";
import { getTickets, getWalletsUser } from "../../api/apiUser";
import phantom from "../../assets/phantom.png";

const HeaderTabsContent = ({ selected }: { selected: string }) => {
  const [search, setSearch] = React.useState("");
  const nav = useHistory();
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const [groupInfo, setGroupInfo] = useState<GroupInfo>();
  const [events, setEvents] = useState<Events[]>();
  const [tickets, setTickets] = useState<any>();
  const [wallet, setWallet] = useState<any>();

  const fetchEvents = async () => {
    const res: any = await getEvents();
    const tickets: any = await getTickets();
    const wallet = (await getWalletsUser()) as { data: { data: "" } };

    setWallet(wallet.data.data);

    setEvents(res.data);
    setTickets(tickets.data.data);
  };

  useEffect(() => {
    fetchEvents();
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <>
      {selected === "afisha" && (
        <Group
          id="tab-content-afisha"
          aria-labelledby="tab-afisha"
          role="tabpanel">
          <div className="bg-search rounded-lg m-4  h-9">
            <Search
              className="h-9"
              value={search}
              onChange={onChange}
              icon={<Icon24Filter />}
            />
          </div>

          {events?.map((item) => {
            return (
              <Group
                separator="hide"
                key={item.category.id}
                header={
                  <Header mode="tertiary">
                    <div className="flex gap-1 items-end">
                      <Text className="text-sm">{item.category.name}</Text>
                      <Text className="text-sub-color">
                        {item.events.length}
                      </Text>
                    </div>
                  </Header>
                }>
                <HorizontalScroll
                  showArrows
                  className="w-full"
                  getScrollToLeft={(i) => i - 224}
                  getScrollToRight={(i) => i + 224}>
                  <div style={{ display: "flex" }}>
                    {item.events.map((item, i) => {
                      return (
                        <HorizontalCell
                          size="l"
                          key={item.id}
                          onClick={() => nav.push(`/afisha/${item.id}`)}
                          header={
                            <div className="flex flex-col">
                              <Title level="3">{item.name}</Title>
                              <Text className="text-xs">
                                {item.description}
                              </Text>
                            </div>
                          }
                          subtitle={"Большой театр · 20.02.2023 в 18:00"}>
                          <img
                            className="w-full h-full object-cover rounded-md"
                            src={item.image}
                          />
                        </HorizontalCell>
                      );
                    })}
                  </div>
                </HorizontalScroll>
              </Group>
            );
          })}
        </Group>
      )}
      {selected === "myTickets" && (
        <Group
          id="tab-content-myTickets"
          aria-labelledby="tab-myTickets"
          role="tabpanel">
          <div className="m-4">
            {tickets &&
              tickets.map((item: any) => (
                <ContentCard
                  key={item.id}
                  onClick={() => {
                    nav.push(`/myTickets/${item.id}`);
                  }}
                  src={item.image}
                  alt={"picture"}
                  subtitle={item.event.category.name}
                  header={item.event.name}
                  text={item.event.description}
                  caption={item.event.location.name}
                  maxHeight={500}
                />
              ))}
          </div>
        </Group>
      )}
      {selected === "profile" && (
        <Group
          id="tab-content-profile"
          aria-labelledby="tab-profile"
          role="tabpanel">
          <Group>
            <div className="m-4">
              <Text className="text-ellipsis">Криптокошельки сообщества</Text>
            </div>
            {wallet?.length > 0 &&
              wallet.map((el: any) => {
                return (
                  <SimpleCell
                    key={el}
                    className={"mb-3"}
                    before={
                      <img
                        width={72}
                        height={72}
                        className={"mr-3 rounded-md"}
                        src={phantom}
                      />
                    }
                    after={<Icon20ChevronRight />}
                    onClick={() => console.log("work")}
                    subtitle={
                      <div className="flex flex-col">
                        <Text className="text-xs text-sub-color">
                          {el?.address}
                        </Text>
                        <Text className="text-xs text-sub-color">
                          {el?.currency?.name}
                        </Text>
                      </div>
                    }>
                    <Title level="2" className="text-base">
                      {el?.name}
                    </Title>
                  </SimpleCell>
                );
              })}
            <CellButton before={<Icon28AddOutline />}>
              <Link
                className="mx-4"
                href={
                  "https://nft-api.maxinteam.ru/wallets/add" +
                  window.location.search
                }
                target="_blank">
                Подключить кошелек
              </Link>
            </CellButton>
          </Group>
        </Group>
      )}
    </>
  );
};

export default HeaderTabsContent;
