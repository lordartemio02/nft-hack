import { push } from "@cteamdev/router";
import { Tabs, TabsItem } from "@vkontakte/vkui";
import React from "react";
import { useHistory } from "react-router-dom";

const HeaderTabs = ({ selected, setSelected }: any) => {
  const nav = useHistory();
  return (
    <Tabs>
      <TabsItem
        selected={selected === "afisha"}
        onClick={() => {
          setSelected("afisha");
          nav.push(`/afisha`);
        }}
        id="tab-afisha"
        aria-controls="tab-content-afisha">
        Афиша
      </TabsItem>
      <TabsItem
        selected={selected === "myTickets"}
        onClick={() => {
          setSelected("myTickets");
          nav.push(`/myTickets`);
        }}
        id="tab-myTickets"
        aria-controls="tab-content-myTickets">
        Мои билеты
      </TabsItem>
      <TabsItem
        selected={selected === "profile"}
        onClick={() => {
          setSelected("profile");
          nav.push(`/profile`);
        }}
        id="tab-profile"
        aria-controls="tab-content-profile">
        Профиль
      </TabsItem>
    </Tabs>
  );
};
export default HeaderTabs;
