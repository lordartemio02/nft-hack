import { back } from "@cteamdev/router";
import { Icon24Filter } from "@vkontakte/icons";
import {
  PanelHeader,
  PanelHeaderBack,
  Search,
  usePlatform,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React from "react";

const CustomSearch = () => {
  const [search, setSearch] = React.useState("");

  const platform = usePlatform();

  const onChange = (e: any) => {
    setSearch(e.target.value);
  };

  // const usersFiltered = users.filter(
  //   ({ name }) => name.toLowerCase().indexOf(search.toLowerCase()) > -1
  // );

  return (
    <PanelHeader
      before={
        platform !== Platform.VKCOM && (
          <PanelHeaderBack
            onClick={() => {
              back();
            }}
          />
        )
      }>
      <Search
        value={search}
        onChange={onChange}
        icon={<Icon24Filter />}
        // onIconClick={onFiltersClick}
      />
    </PanelHeader>
  );
};

export default CustomSearch;
