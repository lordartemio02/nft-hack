import { View } from "@cteamdev/router";
import { useSetAtomState } from "@mntm/precoil";
import bridge from "@vkontakte/vk-bridge-mock";
import { BrowserRouter, Route } from "react-router-dom";
import {
  AdaptivityProvider,
  AppRoot,
  ConfigProvider,
  PlatformType,
} from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";
import React, { FC, useEffect } from "react";
import { Navigation } from "./components/Navigation";
import { Home, Info } from "./pages";
import Admin from "./pages/Admin";
import Profile from "./pages/Profile";
import { getPlatform } from "./services";
import { userInfoAtom, vkUserAtom } from "./store";
import "./index.css";
import AddEvent from "./pages/AddEvent";
import Requests from "./pages/Requests";
import Afisha from "./pages/Afisha";
import axios from "axios";
import { authUser, startAuth } from "./api/apiUser";
import Event from "./pages/Event";
import AdminCommunity from "./pages/AdminCommunity";
import MyTickets from "./pages/MyTickets";
import Ticket from "./pages/Ticket";

export const App: FC = () => {
  const platform: PlatformType = getPlatform();
  const setVkUser = useSetAtomState(vkUserAtom);
  const setUser = useSetAtomState(userInfoAtom);

  const load = async () => {
    bridge
      .send("VKWebAppGetUserInfo")
      .then((el) => {
        setVkUser(el);
      })
      .catch((e) => console.log({ error: e }));

    const user = (await startAuth()) as { data: { data: "" } };
    setUser(user && user.data.data);
  };

  useEffect(() => {
    load();
  }, []);

  return (
    <ConfigProvider platform={platform}>
      <AdaptivityProvider>
        <BrowserRouter>
          <Route path="/" exact component={() => <Afisha />} />
          <Route path="/afisha" exact component={() => <Afisha nav="/" />} />
          <Route
            path="/afisha/:idEvent"
            component={() => <Event nav="/afisha/:idEvent" />}
          />
          <Route path="/info" exact component={() => <Info nav="/" />} />
          <Route
            path="/profile"
            exact
            component={() => <Profile nav="/profile" />}
          />
          <Route path="/admin" exact component={() => <Admin nav="/admin" />} />
          <Route
            path="/admin/community"
            exact
            component={() => <AdminCommunity nav="/admin/community" />}
          />
          <Route
            path="/event"
            exact
            component={() => <AddEvent nav="/event" />}
          />
          <Route
            path="/event/requests"
            exact
            component={() => <Requests nav="/event/request" />}
          />
          <Route
            path="/myTickets"
            exact
            component={() => <MyTickets nav="/myTickets" />}
          />
          <Route
            path="/myTickets/:ticketId"
            exact
            component={() => <Ticket nav="/myTickets/:ticketId" />}
          />
        </BrowserRouter>
      </AdaptivityProvider>
    </ConfigProvider>
  );
};
