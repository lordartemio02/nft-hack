import bridge from "@vkontakte/vk-bridge";

// bridge.subscribe((e: VKBridgeEvent<AnyReceiveMethodName>) => {
//   if (e.detail.type === "VKWebAppUpdateConfig") {
//     const scheme: string = e.detail.data.scheme || "client_light";
//     document.body.setAttribute("scheme", scheme);
//   }
// });

bridge.subscribe((e) => console.log(e));

bridge.send("VKWebAppInit", {}).catch((e) => console.log(e));
