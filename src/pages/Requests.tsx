import {
  Icon12ChevronOutline,
  Icon16TicketOutline,
  Icon20ChevronRight,
  Icon20TicketOutline,
  Icon24ChevronRight,
  Icon24InfoCircleOutline,
  Icon24MessageOutline,
  Icon24UserAddOutline,
  Icon28AddOutline,
  Icon28QrCodeOutline,
  Icon28TicketOutline,
  Icon36MessageOutline,
} from "@vkontakte/icons";
import { GroupInfo, UserInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Button,
  ButtonGroup,
  CellButton,
  Counter,
  Div,
  Group,
  Header,
  IconButton,
  Link,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  platform,
  RichCell,
  SimpleCell,
  Subhead,
  Switch,
  Tabs,
  TabsItem,
  Text,
  Title,
  usePlatform,
  UsersStack,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React, { useEffect, useState } from "react";

const Requests = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("newReq");
  const [groupInfo, setGroupInfo] = useState<UserInfo>();
  const lengthArray = ["some", "some", "some", "some"];

  useEffect(() => {
    bridgeMock.send("VKWebAppGetUserInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader before={<PanelHeaderBack />} after={<Avatar size={36} />}>
        <DefaultInPanel selected={selected} setSelected={setSelected} />
      </PanelHeader>
      <Group separator="hide">
        <EventsInPanel selected={selected} setSelected={setSelected} />
      </Group>

      {selected === "newReq" && (
        <Group
          separator="hide"
          id="tab-content-newReq"
          aria-labelledby="tab-newReq"
          role="tabpanel">
          {lengthArray.map((el, i) => (
            <RichCell
              key={el + i}
              before={<Avatar size={72} src={groupInfo?.photo_200} />}
              after={<Icon36MessageOutline />}
              caption={
                <div className="flex flex-col">
                  <div className="flex gap-1.5">
                    <Icon16TicketOutline />
                    <span>сегодня в 18:00</span>
                  </div>
                  <div className="flex gap-1.5">
                    <Icon16TicketOutline />
                    <span>сегодня в 18:00</span>
                  </div>
                </div>
              }
              actions={
                <ButtonGroup mode="horizontal" gap="s" stretched>
                  <Button mode="primary" size="s">
                    Добавить
                  </Button>
                  <Button mode="secondary" size="s">
                    Скрыть
                  </Button>
                </ButtonGroup>
              }
              disabled>
              Ром Захаров
            </RichCell>
          ))}
        </Group>
      )}
      {selected === "acceptedReq" && (
        <Group
          separator="hide"
          id="tab-content-acceptedReq"
          aria-labelledby="tab-acceptedReq"
          role="tabpanel">
          <Div>Контент acceptedReq</Div>
        </Group>
      )}
      {selected === "declineReq" && (
        <Group
          separator="hide"
          id="tab-content-declineReq"
          aria-labelledby="tab-declineReq"
          role="tabpanel">
          <Div>Контент declineReq</Div>
        </Group>
      )}
      {selected === "allReq" && (
        <Group
          separator="hide"
          id="tab-content-allReq"
          aria-labelledby="tab-allReq"
          role="tabpanel">
          <Div>Контент allReq</Div>
        </Group>
      )}
    </Panel>
  );
};

const DefaultInPanel = ({ selected, setSelected }: any) => {
  return (
    <Tabs>
      <TabsItem
        selected={selected === "afisha"}
        onClick={() => {
          setSelected("afisha");
        }}
        id="tab-afisha"
        aria-controls="tab-content-afisha">
        Афиша
      </TabsItem>
      <TabsItem
        selected={selected === "myTickets"}
        onClick={() => {
          setSelected("myTickets");
        }}
        id="tab-myTickets"
        aria-controls="tab-content-myTickets">
        Мои билеты
      </TabsItem>
      <TabsItem
        selected={selected === "profile"}
        onClick={() => {
          setSelected("profile");
        }}
        id="tab-profile"
        aria-controls="tab-content-profile">
        Профиль
      </TabsItem>
    </Tabs>
  );
};

const EventsInPanel = ({ selected, setSelected }: any) => {
  return (
    <Tabs mode="secondary">
      <TabsItem
        selected={selected === "newReq"}
        onClick={() => {
          setSelected("newReq");
        }}
        id="tab-newReq"
        aria-controls="tab-content-newReq">
        Новые
      </TabsItem>
      <TabsItem
        selected={selected === "acceptedReq"}
        onClick={() => {
          setSelected("acceptedReq");
        }}
        id="tab-acceptedReq"
        aria-controls="tab-content-acceptedReq">
        Принятые
      </TabsItem>
      <TabsItem
        selected={selected === "declineReq"}
        onClick={() => {
          setSelected("declineReq");
        }}
        id="tab-declineReq"
        aria-controls="tab-content-declineReq">
        Отклоненные
      </TabsItem>
      <TabsItem
        selected={selected === "allReq"}
        onClick={() => {
          setSelected("allReq");
        }}
        id="tab-allReq"
        aria-controls="tab-content-allReq">
        Все
      </TabsItem>
    </Tabs>
  );
};

export default Requests;
