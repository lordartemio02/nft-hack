import { push } from "@cteamdev/router";
import { useAtomValue } from "@mntm/precoil";
import {
  Icon28ArticleOutline,
  Icon28BillheadOutline,
  Icon28CancelCircleOutline,
  Icon28CheckCircleOutline,
  Icon28ChevronRightOutline,
  Icon28PawOutline,
  Icon28WarningTriangleOutline,
} from "@vkontakte/icons";
import { UserInfo } from "@vkontakte/vk-bridge";
import {
  Avatar,
  Group,
  Panel,
  PanelHeader,
  PanelProps,
  SimpleCell,
} from "@vkontakte/vkui";
import React from "react";
import QRCode from "react-qr-code";
import { setDoneSnackbar, setErrorSnackbar } from "../hooks";
import { converterFromBase64 } from "../services/converterBase64";
import { vkUserAtom } from "../store";

export const Home: React.FC<PanelProps> = ({ nav }: PanelProps) => {
  const vkUser: UserInfo = useAtomValue(vkUserAtom);

  return (
    <Panel nav={nav}>
      <PanelHeader>Главная</PanelHeader>
    </Panel>
  );
};
