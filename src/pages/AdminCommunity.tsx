import { Icon20ChevronRight, Icon20TicketOutline } from "@vkontakte/icons";
import {
  Avatar,
  Counter,
  Group,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  SimpleCell,
  Text,
  Title,
} from "@vkontakte/vkui";
import React, { useEffect, useState } from "react";
import { getWalletsUser, startAuth } from "../api/apiUser";
import HeaderTabs from "../components/HeaderTabs";
import { useHistory } from "react-router-dom";

const AdminCommunity = ({ nav }: PanelProps) => {
  const [wallet, setWallet] = useState<any>();
  const [selected, setSelected] = useState("profile");

  const navig = useHistory();

  const fetchData = async () => {
    const wallet = (await getWalletsUser()) as { data: { data: "" } };

    setWallet(wallet.data.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader
        before={<PanelHeaderBack onClick={() => navig.goBack()} />}
        after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>
      <Group>
        <Text>Криптокошельки сообщества</Text>
        {wallet?.length > 0 &&
          wallet.map((el: any) => {
            return (
              <SimpleCell
                key={el}
                className={"mb-3"}
                before={
                  <img
                    width={72}
                    height={72}
                    className={"mr-3 rounded-md"}
                    src={"../assets/phantom.jpg"}
                  />
                }
                after={<Icon20ChevronRight />}
                onClick={() => console.log("work")}
                subtitle={
                  <div className="flex flex-col">
                    <Text className="text-xs text-sub-color">
                      {el?.address}
                    </Text>
                    <Text className="text-xs text-sub-color">
                      {el?.currency?.name}
                    </Text>
                  </div>
                }>
                <Title level="2" className="text-base">
                  {el?.name}
                </Title>
              </SimpleCell>
            );
          })}
      </Group>
    </Panel>
  );
};

export default AdminCommunity;
