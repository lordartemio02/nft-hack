import { push } from "@cteamdev/router";
import {
  Icon12ChevronOutline,
  Icon20ChevronRight,
  Icon20TicketOutline,
  Icon24ChevronRight,
  Icon24InfoCircleOutline,
  Icon28AddOutline,
  Icon28QrCodeOutline,
  Icon28TicketOutline,
} from "@vkontakte/icons";
import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Button,
  CellButton,
  Counter,
  Div,
  Group,
  Header,
  IconButton,
  Link,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  platform,
  SimpleCell,
  Subhead,
  Switch,
  Tabs,
  TabsItem,
  Text,
  Title,
  usePlatform,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React, { useEffect, useState } from "react";
import HeaderTabs from "../components/HeaderTabs";

import { useHistory } from "react-router-dom";

const AddEvent = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("allEvents");
  const [groupInfo, setGroupInfo] = useState<GroupInfo>();
  const platform = usePlatform();
  const lengthArray = ["some", "some", "some"];
  const navig = useHistory();

  useEffect(() => {
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader
        before={<PanelHeaderBack onClick={() => navig.goBack()} />}
        after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>
      <Group separator="hide">
        <SimpleCell
          className={"mb-3"}
          before={
            <img
              width={72}
              height={72}
              className={"mr-3 rounded-md"}
              src={groupInfo?.photo_200}
            />
          }
          after={<Icon20ChevronRight />}
          onClick={() => console.log("work")}
          subtitle={
            <Text className="text-sm text-sub-color">
              {groupInfo?.description}
            </Text>
          }>
          <Title level="2" className="text-base">
            {groupInfo?.name}
          </Title>
        </SimpleCell>
      </Group>
      <Group
        className="pb-0"
        padding="s"
        description={
          <div className="flex flex-col">
            <Subhead>
              Для использования в мини-приложениях, Delivery Club, VK Taxi и
              других сервисах ВКонтакте. Эти адреса видны только Вам.
            </Subhead>
            <CellButton className="p-0">Скопировать ссылку</CellButton>
            <CellButton className="p-0">Создать QR-код</CellButton>
          </div>
        }>
        <SimpleCell
          before={<Icon28TicketOutline />}
          after={
            <IconButton>
              <Icon24InfoCircleOutline />
            </IconButton>
          }
          subtitle="Команда ВКонтакте">
          Продано 4/25
        </SimpleCell>
        <SimpleCell
          before={<Icon28QrCodeOutline />}
          after={
            <IconButton>
              <Icon24ChevronRight />
            </IconButton>
          }>
          Продано 4/25
        </SimpleCell>
        <SimpleCell Component="label" after={<Switch defaultChecked />}>
          Событие активно
        </SimpleCell>
        <SimpleCell Component="label" after={<Switch defaultChecked />}>
          Частое событие
        </SimpleCell>
      </Group>
      <Group>
        <Header
          aside={
            <Link onClick={() => navig.push("/event/requests")}>
              Показать все
              {platform === Platform.VKCOM && <Icon12ChevronOutline />}
            </Link>
          }>
          <div className="flex gap-2 items-end">
            <Title level="2">Заявки</Title>
            <Text className="text-[#818C99]">3</Text>
            <Counter size="s" mode="primary">
              3
            </Counter>
          </div>
        </Header>
      </Group>
      <Group>
        {lengthArray.map((el, i) => (
          <SimpleCell
            key={el + i}
            className={"mb-3"}
            before={
              <img
                width={72}
                height={72}
                className={"mr-3 rounded-md"}
                src={groupInfo?.photo_200}
              />
            }
            after={<Icon20ChevronRight />}
            onClick={() => console.log("work")}
            subtitle={
              <div className="flex flex-col">
                <Text className="text-xs text-sub-color">
                  {groupInfo?.description}
                </Text>
                <div className="flex">
                  <Icon20TicketOutline />
                  <Text className="text-xs text-sub-color">1/25 на 8000 ₽</Text>
                </div>
              </div>
            }>
            <Title level="2" className="text-base">
              {groupInfo?.name}
            </Title>
          </SimpleCell>
        ))}
      </Group>
    </Panel>
  );
};

export default AddEvent;
