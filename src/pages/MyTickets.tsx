import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Button,
  CellButton,
  Counter,
  Div,
  Group,
  Header,
  IconButton,
  Link,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  platform,
  SimpleCell,
  Subhead,
  Switch,
  Tabs,
  TabsItem,
  Text,
  Title,
  usePlatform,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React, { useEffect, useState } from "react";
import { getEvents } from "../api/apiAfisha";
import HeaderTabs from "../components/HeaderTabs";
import HeaderTabsContent from "../components/HeaderTabs/HeaderTabsContent";
import { useHistory } from "react-router-dom";

const MyTickets = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("myTickets");
  const [groupInfo, setGroupInfo] = useState<GroupInfo>();
  const platform = usePlatform();
  const lengthArray = ["some", "some", "some"];

  const navigation = useHistory();

  useEffect(() => {
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader before={<PanelHeaderBack />} after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>

      <HeaderTabsContent selected={selected} />
    </Panel>
  );
};

export default MyTickets;
