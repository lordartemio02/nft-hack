import { useAtomValue } from "@mntm/precoil";
import { Icon24Back } from "@vkontakte/icons";
import {
  Avatar,
  Banner,
  Button,
  Div,
  Group,
  Header,
  Link,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  Tabs,
  TabsItem,
} from "@vkontakte/vkui";
import React, { useState } from "react";
import HeaderTabs from "../components/HeaderTabs";
import HeaderTabsContent from "../components/HeaderTabs/HeaderTabsContent";
import { userInfoAtom } from "../store";
import { useHistory } from "react-router-dom";

const Profile = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("profile");
  const navigation = useHistory();

  return (
    <Panel nav={nav}>
      <PanelHeader
        before={<PanelHeaderBack onClick={() => navigation.goBack()} />}
        after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>

      <HeaderTabsContent selected={selected} />
    </Panel>
  );
};

export default Profile;
