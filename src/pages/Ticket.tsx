import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Button,
  Caption,
  CellButton,
  Counter,
  Div,
  Group,
  Header,
  IconButton,
  Link,
  MiniInfoCell,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  platform,
  SimpleCell,
  Subhead,
  Switch,
  Tabs,
  TabsItem,
  Text,
  Title,
  usePlatform,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React, { useEffect, useState } from "react";
import { getEvents } from "../api/apiAfisha";
import HeaderTabs from "../components/HeaderTabs";
import HeaderTabsContent from "../components/HeaderTabs/HeaderTabsContent";
import { useParams } from "react-router-dom";
import { getTicket } from "../api/apiUser";
import {
  Icon20CalendarOutline,
  Icon20MoneyCircleOutline,
  Icon20PlaceOutline,
  Icon28CalendarAddOutline,
  Icon28CalendarCircleFillRed,
} from "@vkontakte/icons";

const Ticket = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("myTickets");
  const [groupInfo, setGroupInfo] = useState<GroupInfo>();

  const { ticketId } = useParams<{ ticketId: string }>();

  const [ticket, setTicket] = useState<any>();

  const fetchData = async () => {
    const response: any = await getTicket(parseInt(ticketId));
    console.log(response);

    setTicket(response.data.data);
  };

  useEffect(() => {
    fetchData();
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader before={<PanelHeaderBack />} after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>

      <Group>
        <div className="relative p-4">
          <div className="rounded-ball-left"></div>
          <img src={ticket?.image} alt="" height={296} className={"w-full"} />
          <div className="rounded-ball-right"></div>
          <div className="horizontal_dotted_line w-full"></div>
        </div>
        <div className="mx-4">
          <Caption>{ticket?.event.category.name}</Caption>
          <Title level="3">{ticket?.event.name}</Title>
        </div>
        <MiniInfoCell before={<Icon20CalendarOutline />}>
          {ticket?.event.date_start}
        </MiniInfoCell>
        <MiniInfoCell before={<Icon20PlaceOutline />}>
          {ticket?.event.location.name}
        </MiniInfoCell>
        <MiniInfoCell before={<Icon20MoneyCircleOutline />}>
          {ticket?.sector.price +
            " " +
            ticket?.sector.sector.price_currency.name}
        </MiniInfoCell>
      </Group>
    </Panel>
  );
};

export default Ticket;
