import { push } from "@cteamdev/router";
import {
  Icon12Verified,
  Icon20AddCircle,
  Icon20AddCircleFillBlue,
  Icon20ChevronRight,
  Icon20PulseOutline,
  Icon20TicketOutline,
  Icon24Back,
  Icon28AddOutline,
  Icon28MessageOutline,
  Icon48UserAddCircleFillBlue,
} from "@vkontakte/icons";
import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Badge,
  Banner,
  Button,
  Counter,
  Div,
  Footnote,
  Group,
  Header,
  IconButton,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  SimpleCell,
  Tabs,
  TabsItem,
  Text,
  Title,
} from "@vkontakte/vkui";
import React, { useEffect, useState } from "react";
import HeaderTabs from "../components/HeaderTabs";
import { useHistory } from "react-router-dom";

const Admin = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("allEvents");
  const [groupInfo, setGroupInfo] = useState<GroupInfo>();
  const lengthArray = ["some", "some", "some"];
  const navig = useHistory();

  useEffect(() => {
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader
        before={<PanelHeaderBack onClick={() => navig.goBack()} />}
        after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>

      <Group
        header={
          <div className="px-5">
            <Text>Text</Text>
          </div>
        }>
        <SimpleCell
          before={<Avatar size={72} src={groupInfo?.photo_200} />}
          after={<Icon20ChevronRight />}
          onClick={() => console.log("work")}
          subtitle="Команда ВКонтакте">
          Сообщество админа
        </SimpleCell>
        <div className="px-5 flex gap-2">
          <Text>События</Text>
          <Text className="text-[#818C99]">3</Text>
        </div>
      </Group>
      <Group separator="hide">
        <EventsInPanel selected={selected} setSelected={setSelected} />
      </Group>

      {selected === "upcomingEvents" && (
        <Group
          separator="hide"
          id="tab-content-upcomingEvents"
          aria-labelledby="tab-upcomingEvents"
          role="tabpanel">
          {lengthArray.map((el, i) => (
            <SimpleCell
              key={el + i}
              className={"mb-3"}
              before={
                <img
                  width={72}
                  height={72}
                  className={"mr-3 rounded-md"}
                  src={groupInfo?.photo_200}
                />
              }
              after={<Icon20ChevronRight />}
              onClick={() => console.log("work")}
              subtitle={
                <div className="flex flex-col">
                  <Text className="text-xs text-sub-color">
                    {groupInfo?.description}
                  </Text>
                  <div className="flex">
                    <Icon20TicketOutline />
                    <Text className="text-xs text-sub-color">
                      1/25 на 8000 ₽
                    </Text>
                  </div>
                </div>
              }>
              <div className="flex gap-1">
                <Title level="2" className="text-base">
                  {groupInfo?.name}
                </Title>
                <Counter mode="primary">3</Counter>
              </div>
            </SimpleCell>
          ))}
        </Group>
      )}
      {selected === "pastEvents" && (
        <Group
          separator="hide"
          id="tab-content-pastEvents"
          aria-labelledby="tab-pastEvents"
          role="tabpanel">
          <Div>Контент pastEvents</Div>
        </Group>
      )}
      {selected === "allEvents" && (
        <Group
          separator="hide"
          id="tab-content-allEvents"
          aria-labelledby="tab-allEvents"
          role="tabpanel">
          <Div>Контент allEvents</Div>
        </Group>
      )}
      <Div className="flex justify-center">
        <IconButton className="bg-plus" onClick={() => navig.push("/event")}>
          <Icon28AddOutline fill="#FFFFFF" />
        </IconButton>
      </Div>
    </Panel>
  );
};

const EventsInPanel = ({ selected, setSelected }: any) => {
  return (
    <Tabs mode="secondary">
      <TabsItem
        selected={selected === "upcomingEvents"}
        onClick={() => {
          setSelected("upcomingEvents");
        }}
        id="tab-upcomingEvents"
        aria-controls="tab-content-upcomingEvents">
        Предстоящие
      </TabsItem>
      <TabsItem
        selected={selected === "pastEvents"}
        onClick={() => {
          setSelected("pastEvents");
        }}
        id="tab-pastEvents"
        aria-controls="tab-content-pastEvents">
        Прошедшие
      </TabsItem>
      <TabsItem
        selected={selected === "allEvents"}
        onClick={() => {
          setSelected("allEvents");
        }}
        id="tab-allEvents"
        aria-controls="tab-content-allEvents">
        Все
      </TabsItem>
    </Tabs>
  );
};

export default Admin;
