import { push } from "@cteamdev/router";
import {
  Icon16Add,
  Icon16Message,
  Icon16Minus,
  Icon16Share,
  Icon20CalendarOutline,
  Icon20FollowersOutline,
  Icon20MessageOutline,
  Icon20PlaceOutline,
  Icon20PulseOutline,
  Icon20TicketOutline,
  Icon28CalendarOutline,
  Icon28PlaceOutline,
} from "@vkontakte/icons";
import { GroupInfo } from "@vkontakte/vk-bridge";
import bridgeMock from "@vkontakte/vk-bridge-mock";
import {
  Avatar,
  Button,
  ButtonGroup,
  Caption,
  CellButton,
  ContentCard,
  Counter,
  Div,
  Group,
  Header,
  IconButton,
  Link,
  MiniInfoCell,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  PanelProps,
  platform,
  SimpleCell,
  Subhead,
  Switch,
  Tabs,
  TabsItem,
  Text,
  Title,
  usePlatform,
} from "@vkontakte/vkui";
import { Platform } from "@vkontakte/vkui/dist/cjs/lib/platform";
import React, { useEffect, useState } from "react";
import { orderCreate } from "../api/apiUser";
import { IEvent } from "../api/Api.interfaces";
import { getEvent, getEvents } from "../api/apiAfisha";
import HeaderTabs from "../components/HeaderTabs";
import HeaderTabsContent from "../components/HeaderTabs/HeaderTabsContent";
import { useParams, useHistory } from "react-router-dom";

const Event = ({ nav }: PanelProps) => {
  const [selected, setSelected] = useState("none");
  const [groupInfo, setGroupInfo] = useState<GroupInfo>();
  const [event, setEvent] = useState<IEvent>();
  const [count, setCount] = useState<{ id: number; count: number }[]>([]);
  const { idEvent } = useParams<{ idEvent: string }>();
  const navig = useHistory();

  const fetchEvent = async () => {
    const res: any = await getEvent(parseInt(idEvent));
    const allCount: { id: number; count: number }[] = [];
    setEvent(res);
    if (res) {
      res["sectors"].forEach((item: any) => {
        allCount.push({
          id: item.id,
          count: 0,
        });
      });
      setCount(allCount);
    }
  };

  useEffect(() => {
    fetchEvent();
    bridgeMock.send("VKWebAppGetGroupInfo").then((el) => setGroupInfo(el));
  }, []);

  return (
    <Panel nav={nav}>
      <PanelHeader
        before={<PanelHeaderBack onClick={() => navig.goBack()} />}
        after={<Avatar size={36} />}>
        <HeaderTabs selected={selected} setSelected={setSelected} />
      </PanelHeader>

      {/* <HeaderTabsContent selected={selected} /> */}

      <Group>
        <img src={event?.image} height={220} className={"w-full"} />
        <div className="mx-4">
          <Caption>{event?.category.name}</Caption>
          <Title>{event?.name}</Title>
          <Text>{event?.description}</Text>
        </div>
        <MiniInfoCell before={<Icon20CalendarOutline />}>
          {event?.date_start}
        </MiniInfoCell>
        <MiniInfoCell before={<Icon20PlaceOutline />}>
          {event?.location.name}
        </MiniInfoCell>
        <div className="mx-4">
          <ButtonGroup stretched>
            <Button before={<Icon16Share />} stretched>
              Поделится
            </Button>
            <Button
              appearance="accent"
              mode="secondary"
              before={<Icon16Message fill="#2688EB" />}
              stretched>
              <span className="text-blue">Организатор</span>
            </Button>
          </ButtonGroup>
        </div>
      </Group>
      <Group separator="hide">
        {event?.sectors.map((item, i) => {
          return (
            <SimpleCell
              key={item.id}
              className={"mb-3"}
              before={
                <img
                  width={72}
                  height={72}
                  className={"mr-3 rounded-md"}
                  src={item.ticket_source_image}
                />
              }
              after={
                <div className="flex items-center">
                  <IconButton
                    onClick={() =>
                      count[i]?.count > 0 &&
                      setCount((val) => {
                        const newArray = [...val];
                        newArray[i].count = val[i].count - 1;
                        return newArray;
                      })
                    }>
                    <Icon16Minus />
                  </IconButton>
                  <Counter>{count[i]?.count}</Counter>
                  <IconButton>
                    <Icon16Add
                      onClick={() =>
                        count[i]?.count < item.max_count &&
                        setCount((val) => {
                          const newArray = [...val];
                          newArray[i].count = val[i].count + 1;
                          return newArray;
                        })
                      }
                    />
                  </IconButton>
                </div>
              }
              onClick={() => console.log("work")}
              subtitle={
                <div className="flex flex-col">
                  <Text className="text-xs text-sub-color line-height-4">
                    {item.price + " " + item.price_currency.name}
                  </Text>
                </div>
              }>
              <Title level="2" className="text-base line-height-4">
                {item?.name}
              </Title>
            </SimpleCell>
          );
        })}
        <div className="mx-4">
          <Button
            onClick={() =>
              orderCreate({ event_id: event?.id || 0, sectoredIds: count })
            }
            stretched>
            Отправить заявку
          </Button>
        </div>
      </Group>
    </Panel>
  );
};

export default Event;
