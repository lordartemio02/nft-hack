import { init, Router } from "@cteamdev/router";
import ReactDOM from "react-dom";
import { App } from "./App";
import "./bridge";
import React from "react";

init();

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);

if (process.env.NODE_ENV === "development") import("./eruda");
